exclusive_skins = {
	["Hume2"] = {name="Hume2", skin="character_0.png", statue="character0"},
	["CalebJ"] = {name="CalebJ", skin="character_-1.png", statue="character_1"},
	["Coram"] = {name="Coram", skin="character_-2.png", statue="character_2"},
	["Josselin"] = {name="Josselin", skin="character_-3.png", statue="character_3"},
	["LadyK"] = {name="Removed", skin="character_-4.png", statue="character_4"},
	["Vikthor"] = {name="Vikthor", skin="character_-5.png", statue="character_5"},
	["TommyBeeBop"] = {name="TommyBeeBop", skin="character_-6.png", statue="character_6"},
	["Sokomine"] = {name="Sokomine", skin="character_-7.png", statue="character_7"},
	["onePlayer"] = {name="onePlayer", skin="character_167.png", statue="character_8"},
	["sivarajan"] = {name="sivarajan", skin="character_572.png", statue="character_9"},
}

minetest.register_on_joinplayer(function(player)
	local es = exclusive_skins[player:get_player_name()]
	if es then
		player:set_properties({
			visual = "mesh",
			textures = {es.skin},
			visual_size = {x=1, y=1},
		})
	end
end)

for k, v in pairs(exclusive_skins) do
	if v.statue then
		minetest.register_node("exclusive_skins:" .. v.statue, {
			description = v.name .. " Statue",
			tiles = {v.skin},
			groups = {dig_immediate = 2},
			paramtype = "light",
			drawtype = "mesh",
			paramtype2 = "facedir",
			mesh = "exclusive_skins_statue.obj",
			is_ground_content = false,
			collision_box = {
				type = "fixed",
				fixed = {-0.5, -0.5, -0.5, 0.5, 1.5, 0.5},
			},
			selection_box = {
				type = "fixed",
				fixed = {-0.5, -0.5, -0.5, 0.5, 1.5, 0.5},
			},
		})
	end
end

